![logo](images/logo.png)

# SwiftCore Batch Api Version History #

## 2.0.3 ##
#### 8th Jan 2020 ####
* a job created in version 1.0 cannot be called/submitted to etc using version 2.0 of the batch api (and vice versa)

## 2.0.2 ##
#### 27th Nov 2019 ####
* Version 2 release
    * Fixed fields replaced by attributes
        * See response models for chunk download
            * [Version1](https://suppress.swiftcore.net/index.html?urls.primaryName=v1.0#/Chunk/GetChunkDownloadAsync)
            * [Version2](https://suppress.swiftcore.net/index.html?urls.primaryName=v2.0#/Chunk/GetChunkDownloadAsync)
    * Get dataset/list returns extra field: dataSetAttributes
* Match level can be set per dataset
* Example code updated to dot net core 3.0
* [Upgrading from vs1 to vs2](batch-upgrade-from-1-to-2.md)
* Swagger updated to OpenAPI 3

## 1.0.22 ##
#### 31st July 2019 ####
* Swagger moved to the root: https://suppress.swiftcore.net

## 1.0.14 ##
#### 30th Jan 2019 ####
* Additional fields
    * NewOccupier: ChunkResponsePersonNewAddress has been expanded to include: NewOccupierT, NewOccupierF, NewOccupierF2 & NewOccupierS
    * Note that NewOccupier remains

## 1.0.12 ##
#### 26th Nov 2018 ####
* Add DataSetRef and DataSetDisplayName to each dataset returned by the dataset/list endpoint
* JobRef, Client Name, CustomerUrn can contain alphanumeric characters and/or following special characters  - . ' , / & # ( ) : only.

## 1.0.11 ##
#### 29th Aug 2018 ####
* Force match type of flag for mover datasets

## 1.0.8 ##
#### 26th Jun 2018 ####
* Added high and low volume processing queue

## 1.0.7 ##
#### 26th Jun 2018 ####
* Added Test mode for user

## 1.0.5 ##
#### 10th Apr 2018 ####
* Added new endpoint [Dataset\Options](https://suppress.swiftcore.net/index.html#/Dataset/GetDatasetOptionsAsync)
* Updated matching logic; now using match 1.2.8

## 1.0.4 ##
#### 19th Mar 2018 ####
* Added Head method to [About\Status](https://suppress.swiftcore.net/index.html#/About/GetStatus)  
* Added stats method [About\Stats](https://suppress.swiftcore.net/index.html#/About/GetStats)
* Updated WaitDuration: a return of -1 indicates that the chunk has failed and will not be returned.

## 1.0.3 ##
#### 5th Mar 2018 ####
* Added new endpoint [Chunks downloaded for a job](https://suppress.swiftcore.net/index.html#/Job/GetJobChunkDownloadedListAsync)  

## 1.0.2 ##
#### 5th Jan 2018 ####
* Moved to a serverless architecture for processing chunks.
    * This provides better scaling & hence throughput.
    * One small issue is that there will be a possible warmup period if the system has been idle for a period of time. 
* WaitDuration implemented
    * Returned in the response header for CreateChunk, GetChunkReport & GetChunkDownload
    * WaitDuration is an estimate of when the chunk will be finished processing expressed in seconds from now.
    * The value is a double (with decimal places)
* Swagger allows entering a bearer token

## 1.0.1 ##
#### 20th Nov 2017 ####
* Initial EAP release

