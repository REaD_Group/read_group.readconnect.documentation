![logo](images/logo.png)

# SwiftCore Single Api Version History #

## 2.0.3 ##
#### 8th Jan 2020 ####
* Rate limiting implemented
    * response code 429 returned when exceeded

## 2.0.2 ##
#### 27th Nov 2019 ####
* V2 release
* New address & new occupier returned as attributes
* Swagger updated to OpenAPI 3

## 1.0.4 ##
#### 7th Aug 2019 ####
* Initial release