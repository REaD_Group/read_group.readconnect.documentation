![Logo](images/logo.png)

# SwiftCore batch suppress api documentation #

## Overview ##
SwiftCore batch suppress is a RESTful api allowing the matching of multiple names and addresses against UK suppression & new address datasets.

Swagger: https://suppress.swiftcore.net/

Example use:

* Get authentication token from our identity server
* Create a job, setting options
* Submit names and addresses in chunks of up to 2000 per chunk
* Optionally, look at the match report
* Download the matches


![QuickStart](images/batch-quick-start.png)

All of these steps are performed by calling respective endpoints.


## Getting started ##
* This git repository contains c# code examples (created using visual studio 2017) for both single & batch api's.
* Clone this repo: `git clone https://bitbucket.org/thesoftwarebureau/swiftcore.documentation.suppress`
* Update the client id & secret in the example code with your client id & secret (see 'Get an api key' below)

    * #### Code ####
        * See [Batch quickStart c# code](https://bitbucket.org/thesoftwarebureau/swiftcore.documentation.suppress/src/master/src/batch/dotnetcore3.0/Documentation.Suppress.Batch/Program.cs?at=master&fileviewer=file-view-default#Program.cs-18)
    * #### Endpoints ####
        * [Create job](https://suppress.swiftcore.net/index.html#/Job/CreateJobAsync)
        * [Submit name & address chunk](https://suppress.swiftcore.net/index.html#/Chunk/CreateChunkAsync)
        * [Get match report](https://suppress.swiftcore.net/index.html#/Chunk/GetChunkReportAsync)

## Authentication ##
* [authentication documentation](authentication.md)

## Terminology ##
* Dataset
    * A list of reference names & addresses whom, for instance, have moved house.
    * See the [dataset endpoint](https://suppress.swiftcore.net/index.html#/Dataset/GetDatasetListAsync) for more detail.
* Job: The wrapper for setting options.  All chunks belong to a job.
* JobOptions: 
    * DataSetIds:
        * the list of datasets you wish to match against
        * the order of the list sets the matching order (ie. match hierarchy)
    * See [JobRequest model](https://bitbucket.org/thesoftwarebureau/swiftcore.documentation.suppress/src/master/src/batch/dotnetcore3.0/Documentation.Suppress.Batch/Models/JobRequest.cs?at=master&fileviewer=file-view-default)
* Chunk: a group of names and addresses.  The chunk can contain 1..2000 names & addresses.

## High & Low Volume Queues ##
* SwiftCore has multiple queues, designed to facilitate optimum performance according to the volume of records being processed.
* SwiftCore will determine which Queue (High or Low Volume) each Chunk is passed to by analysing the total number of records in the Chunk.
    * Low Volume (<10,000 records) - Chunk Size = 200 records
    * High Volume (>10,000 records) - Chunk Size = 2,000 records

    ![QuickStart](images/queues.png)

## Use Cases ##
* #### Health Check Use Case ####
    ![HealthCheckFlow](images/health-check-flow.png)

    * Example of an interactive workflow:
        * [Create job](https://suppress.swiftcore.net/index.html#/Job/CreateJobAsync)
        * [Submit name & address chunk](https://suppress.swiftcore.net/index.html#/Chunk/CreateChunkAsync)
        * [Get match report](https://suppress.swiftcore.net/index.html#/Chunk/Chunk_GetChunkReportAsync)
        * [Change dataset hierarchy](https://suppress.swiftcore.net/index.html#/Job/Job_UpdateJobAsync)
        * [Get match report](https://suppress.swiftcore.net/index.html#/Chunk/Chunk_GetChunkReportAsync)
        * [Download matches](https://suppress.swiftcore.net/index.html#/Chunk/Chunk_GetChunkDownloadAsync)  

* #### Integrated Download Use Case ####

    ![IntegratedFlow](images/integrated-flow.png)

    * Example of a non-interactive/automated workflow:
        * [Create job](https://suppress.swiftcore.net/index.html#/Job/Job_CreateJobAsync)
        * [Submit name & address chunk](https://suppress.swiftcore.net/index.html#/Chunk/Chunk_CreateChunkAsync)
        * [Download matches](https://suppress.swiftcore.net/index.html#/Chunk/Chunk_GetChunkDownloadAsync)

## Batch Job lifespan ##
* A batch job will be automatically closed 5 days after creation.
    * Any charges (for match downloads) will be billed at this point.
    * Upon job close, no more chunks can be submitted.
* A batch job will remain active for 10 days after it has been closed.
    * this allows for downloading or reporting during that period.

## Matching ##
* [matching documentation](matching.md)

## Attributes ##
* [attributes documentation](attributes.md)

## Test Mode ##
* [test mode documentation](test-mode.md)

## Limitations ##
* There is a time to live on the authentication token of 1 hour
* Every name & address within a chunk must have a unique CustomerUrn (we will return CustomerUrn within the download).
* If you change the dataset list hierarchy, you will need to call any previously called chunk reports to reflect the updated hierarchy.
* The match level cannot be changed after the job has been created.
* There is no address level only match level.
* The dataset list hierarchy cannot be changed after downloading any chunk (calling the chunk download endpoint).

## FAQs ##
* [faq's](faqs.md)

## Versions ##
* Current version: https://suppress.swiftcore.net/api/v2.0/about/version
* [Version history](batch-versions.md)
* [Upgrading from vs1 to vs2](batch-upgrade-from-1-to-2.md)
