![Logo](images/logo.png)

# SwiftCore suppress api attributes #

## About ##
When downloading a chunk (in batch) or getting the match results from a single api call, if the dataset that has been matched has additional data to be returned eg. new occupier or new address.  This information is now (as of V2) returned as attributes.  The attributes are returned as an array of key value pairs.

## Example ##
* below is a json example of a single api response
* it shows a match to the DEMO dataset with 2 attributes:
    * NewAddress attribute with a value of: 1234 NEW ROAD, NEWTOWN, AA0 0XX
    * NewOccupierFullname attribute with a value of: Mr Fred Smith

```json
    {
      "jobId": "18871b97-036c-4fa8-8b95-7f9c2f41f661",
      "customerUrn": "57804ab3-312b-4726-9401-83a2cb9a2e5b",
      "datasets": [
        {
          "datasetId": "DEMO",
          "matchLevel": "IndividualLoose",
          "attributes": [
            {
              "key": "NewAddress",
              "value": ["1234 NEW ROAD", "NEWTOWN", "AA0 0XX"]
            },
            {
              "key": "NewOccupierFullname",
              "value": ["Mr Fred Smith"]
            }
          ]
        }
      ]
    }
```

## Valid attribute keys ##
* NewAddress
* NewOccupierFullname
* NewOccupierTitle
* NewOccupierForename
* NewOccupierMiddlename
* NewOccupierSurname

## Notes ##
* Attributes are returned when matching to a dataset; the dataset dictates the attributes returned
* Every dataset holds different attributes (or none)
* To see what attributes a dataset can possibly return; see the [batch dataset/list endpoint](https://suppress.swiftcore.net/index.html#/Dataset/GetDatasetListAsync)

