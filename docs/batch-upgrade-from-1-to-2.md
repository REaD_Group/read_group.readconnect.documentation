![Logo](images/logo.png)

# REaDConnect suppress upgrading from vs1 to vs2 #

# vs1 will be retired 9am BST 3rd August 2020

## Api changes ##
* Get dataset list response
    * returns an extra field: dataSetAttributes
    * an array of attributes that the dataset may return
    * Endpoint effected: [Get Dataset/list](https://suppress.swiftcore.net/index.html#/Dataset/GetDatasetListAsync)
* Match level is set per dataset
    * In version 1, the matchLevel was set for the job.
    * In version 2, the matchLevel is now set at the dataset level.
    * Note that the [match levels](matching.md) are unchanged
    * Endpoints effected: [Post/Put Job](https://suppress.swiftcore.net/index.html#/Job/CreateJobAsync)
* Attributes returned replacing old fixed fields
    * In version 1, the NewAddress & NewOccupier fields were part of the response model
    * In version 2, the NewAddress & NewOccupier are returned as attributes within the response model
    * [Attributes documentation](attributes.md)
    * Endpoint effected: [Get Chunk/download](https://suppress.swiftcore.net/index.html#/Chunk/GetChunkDownloadAsync)

## Miscellaneous changes ##
* Swagger updated to OpenAPI 3
* Example code updated to dot net core 3.0