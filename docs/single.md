![Logo](images/logo.png)

# SwiftCore single suppress api documentation #

## Overview ##
SwiftCore single suppress is a RESTful api allowing the matching of a single name and addresses against UK suppression & new address datasets.
Swagger: https://suppress-single.swiftcore.net/

Example use:

1. Get access token from our identity server
2. Post name and addresses & receive the matches in the response
3. Make next post re-using the token

![QuickStart](images/single-quick-start.png)

All of these steps are performed by calling respective endpoints.


## Getting started ##
* This git repository contains c# code examples (created using visual studio 2017) for both single & batch api's
* Clone this repo: `git clone https://bitbucket.org/thesoftwarebureau/swiftcore.documentation.suppress`
* Update the client id & secret in the example code with your client id & secret (see 'Get an api key' below)

    * #### Code ####
        * See [Single c# code](https://bitbucket.org/thesoftwarebureau/swiftcore.documentation.suppress/src/master/src/single/dotnetcore3.0/Documentation.Suppress.Single/Program.cs?at=master&fileviewer=file-view-default#Program.cs-17)
    * #### Endpoints ####
        * [Submit single name & address](https://suppress-single.swiftcore.net/index.html#/Single/SingleMatchPostRequestAsync)

## Authentication ##
* [authentication documentation](authentication.md)

## Terminology ##
* Dataset
    * A list of reference names & addresses whom, for instance, have moved house.
    * See the [get datasets endpoint](https://suppress-single.swiftcore.net/index.html#/Dataset/GetDatasetListAsync) for more detail
* Dataset Attributes
    * These are attributes that are returned for relevant datasets.
    * See the 'DatasetServiceDescription' for each dataset (see [datasets endpoint](https://suppress-single.swiftcore.net/index.html#/Dataset/GetDatasetListAsync)) and then compare to the below table describing which attributes will be returned
 
        |DatasetServiceDescription|Attribute count|Attribute keys returned|
        |---|---|---|
        |New Occupier|5|NewOccupierFullname, NewOccupierTitle, NewOccupierForename, NewOccupierMiddlename, NewOccupierSurname|
        |Relocation|1|NewAddress|
    
    * Notes
        * the order of attributes returned is by key (alphabetical)
        * the attribute value is an array of strings

* Get Results
    * if you do not receive the response (due to network problem) then you can call the single/get endpoint passing the CustomerUrn
    * See the [get single endpoint](https://suppress-single.swiftcore.net/index.html#/Single/get_api_v2_0_single__customerUrn_) for more detail

## Limitations ##
* There is a time to live on the authentication token of 1 hour
* CustomerUrn must be unique for all of your calls

## Matching ##
* [matching documentation](matching.md)

## Attributes ##
* [attributes documentation](attributes.md)

## Test Mode ##
* [test mode documentation](test-mode.md)

## FAQs ##
* [faq's](faqs.md)

## Versions ##
* Current version: https://suppress-single.swiftcore.net/api/v2.0/about/version
* [Version history](single-versions.md)
