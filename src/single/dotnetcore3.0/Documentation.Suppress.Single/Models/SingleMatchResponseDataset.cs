﻿using System.Collections.Generic;

namespace Documentation.Suppress.Single.Models
{
    /// <summary>
    /// Represents a matched dataset for the single api
    /// </summary>
    public class SingleMatchResponseDataset
    {
        public SingleMatchResponseDataset()
        {
            Attributes = new List<SingleMatchResponseDatasetAttribute>();
        }

        public string DatasetId { get; set; }
        public List<SingleMatchResponseDatasetAttribute> Attributes { get; set; }
    }
}
