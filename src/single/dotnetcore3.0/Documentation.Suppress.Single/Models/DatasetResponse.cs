﻿using System.Collections.Generic;

namespace Documentation.Suppress.Single.Models
{
    public class DatasetResponse
    {
        public string DataSetId { get; set; }
        public string DatasetName { get; set; }
        public string DatasetDescription { get; set; }
        public string DatasetServiceDescription { get; set; }
        public string DataSetDisplayName { get; set; }
        public string SupplierName { get; set; }
        public string DataSetRef { get; set; }
        public List<string> DataSetAttributes { get; set; }
    }
}