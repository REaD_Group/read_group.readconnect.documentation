﻿namespace Documentation.Suppress.Single.Models
{
    public class SwiftCoreApiError
    {
        public string Message { get; set; }
    }
}
