﻿using System;
using System.Collections.Generic;

namespace Documentation.Suppress.Single.Models
{
    public class SingleMatchResponse
    {
        public Guid JobId { get; set; }
        public string CustomerUrn { get; set; }
        public List<SingleMatchResponseDataset> Datasets { get; set; }
    }
}