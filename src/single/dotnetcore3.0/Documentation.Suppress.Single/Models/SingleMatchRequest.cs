﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Documentation.Suppress.Single.Models
{
    public class SingleMatchRequest
    {
        [Required]
        [StringLength(maximumLength: 50)]
        public string CustomerUrn { get; set; }
        [StringLength(maximumLength: 50)]
        public string ClientName { get; set; }
        public int MaxResponseDatasets { get; set; }
        [Required]
        public List<DatasetRequest> DataSets { get; set; }
        public string Title { get; set; }
        public string Forename1 { get; set; }
        public string Forename2 { get; set; }
        public string Surname { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Address5 { get; set; }
        public string Address6 { get; set; }
        public string Address7 { get; set; }
        public string Address8 { get; set; }
        public string Address9 { get; set; }
    }
}