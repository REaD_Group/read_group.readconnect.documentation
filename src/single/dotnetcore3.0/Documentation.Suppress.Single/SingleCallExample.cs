﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Documentation.Suppress.Single.Models;
using IdentityModel;
using IdentityModel.Client;
using System.Text.Json;

namespace Documentation.Suppress.Single
{
    public class SingleCallExample
    {
        private const string IdentityServerClientId = @"YOUR-CLIENT-ID-HERE";        // TODO: Set your client id & secret 
        private const string IdentityServerClientSecret = @"YOUR-CLIENT-SECRET-HERE";

        private const string IdentityServerUrl = "https://id.swiftcore.net";
        private const string IdentityServerSingleApiScope = "suppress.single.execute";

        private const string ApiUrl = "https://suppress-single.swiftcore.net/api/v2.0";


        public async Task<TokenResponse> GetAccessTokenFromIdentityServerAsync()
        {
            using (var httpClient = new HttpClient())
            {
                var disco = await httpClient.GetDiscoveryDocumentAsync(IdentityServerUrl);
                if (disco.IsError) throw new Exception(disco.Error);

                var tokenRequest = new TokenRequest
                {
                    Address      = disco.TokenEndpoint,
                    ClientId     = IdentityServerClientId,
                    ClientSecret = IdentityServerClientSecret,
                    GrantType    = OidcConstants.GrantTypes.ClientCredentials,
                    Parameters   = { { "scope", IdentityServerSingleApiScope } }
                };

                var tokenResponse = await httpClient.RequestTokenAsync(tokenRequest);
                if (tokenResponse.IsError) throw new ApplicationException($"Authentication failed: {tokenResponse.Error}");

                return tokenResponse;
            }
        }


        public async Task GetVersionAsync()
        {
            using (var httpClient = new HttpClient())
            {
                string url = $"{ApiUrl}/about/version";
                var response = await httpClient.GetAsync(url);
                if (!response.IsSuccessStatusCode) throw new ApplicationException("Single api call failed");

                var version  = await response.Content.ReadAsAsync<string>();
                Console.WriteLine($"Version: {version}");
            }
        }


        public async Task GetDatasetListAsync(TokenResponse tokenResponse)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.SetBearerToken(tokenResponse.AccessToken);

                var url      = $"{ApiUrl}/dataset/list";
                var response = await httpClient.GetAsync(url);
                if (!response.IsSuccessStatusCode)
                {
                    var error = await response.Content.ReadAsAsync<SwiftCoreApiError>();
                    throw new ApplicationException("Single api call failed: " + error.Message);
                }

                var datasets = await response.Content.ReadAsAsync<List<DatasetResponse>>();
                Console.WriteLine($"Available Datasets: {datasets.Count}");
            }
        }


        public async Task PostSingleMatchAsync(TokenResponse tokenResponse)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.SetBearerToken(tokenResponse.AccessToken);

                var request = new SingleMatchRequest
                {
                    CustomerUrn  = Guid.NewGuid().ToString(),
                    ClientName   = "YourClient",
                    // See swagger for MatchLevel options
                    DataSets = new List<DatasetRequest>
                    {
                        new DatasetRequest(){DataSetId = "REAB", MatchLevel = "IndividualLoose"},
                        new DatasetRequest(){DataSetId = "REAM", MatchLevel = "Surname"},
                        new DatasetRequest(){DataSetId = "REAA", MatchLevel = "IndividualModerate"},
                        new DatasetRequest(){DataSetId = "REAO", MatchLevel = "IndividualLoose"},
                        new DatasetRequest(){DataSetId = "REAT", MatchLevel = "IndividualLoose"}
                    },
                    Forename1    = "Fred",
                    Surname      = "Smith",
                    Address1     = "1 HIGH STREET",
                    Address2     = "AB1 1DC"
                };
                
                var stringPayload = await Task.Run(() => JsonSerializer.Serialize(request));
                var httpContent   = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                var url           = $"{ApiUrl}/single";
                var response      = await httpClient.PostAsync(url, httpContent);
                if (!response.IsSuccessStatusCode)
                {
                    var error = await response.Content.ReadAsAsync<SwiftCoreApiError>();
                    throw new ApplicationException("Single api call failed: "+ error.Message);
                }
                
                var matchResponse = await response.Content.ReadAsAsync<SingleMatchResponse>();
                Console.WriteLine($"Matched Datasets: {matchResponse.Datasets.Count}");
            }
        }


    }
}
