﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IdentityModel.Client;
using IdentityModel;
using SwiftCore.Documentation.Suppress.Batch.Models;
using System.Text.Json;

namespace SwiftCore.Documentation.Suppress.Batch
{
    internal class SuppressExample
    {

        private const string IdentityServerClientId = @"YOUR-CLIENT-ID-HERE"; // TODO: Set your client id & secret 
        private const string IdentityServerClientSecret = @"YOUR-CLIENT-SECRET-HERE";
      
        private const string IdentityServerUrl = "https://id.swiftcore.net";
        private const string IdentityServerBatchApiScope = "suppress.execute";

        private const string ApiUrl = "https://suppress.swiftcore.net/api/v2.0";



        public async Task GetVersion()
        {
            using (var httpClient = new HttpClient())
            {
                string url = $"{ApiUrl}/about/version";
                var response = await httpClient.GetAsync(url);
                if (!response.IsSuccessStatusCode) throw new ApplicationException("Api call failed");

                var version = await response.Content.ReadAsAsync<string>();
                Console.WriteLine($"Version: {version}");
            }
        }


        public async Task<TokenResponse> GetAccessTokenFromIdentityServerAsync()
        {
            using (var httpClient = new HttpClient())
            {
                var disco = await httpClient.GetDiscoveryDocumentAsync(IdentityServerUrl);
                if (disco.IsError) throw new Exception(disco.Error);

                var tokenRequest = new TokenRequest
                {
                    Address = disco.TokenEndpoint,
                    ClientId = IdentityServerClientId,
                    ClientSecret = IdentityServerClientSecret,
                    GrantType = OidcConstants.GrantTypes.ClientCredentials,
                    Parameters = {{"scope", IdentityServerBatchApiScope}}
                };

                var tokenResponse = await httpClient.RequestTokenAsync(tokenRequest);
                if (tokenResponse.IsError)
                    throw new ApplicationException($"Authentication failed: {tokenResponse.Error}");

                return tokenResponse;
            }
        }


        public async Task<List<DatasetResponse>> GetDatasetList(TokenResponse tokenResponse)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.SetBearerToken(tokenResponse.AccessToken);
                var url = $"{ApiUrl}/dataset/list";
                var response = await httpClient.GetAsync(url);

                if (!response.IsSuccessStatusCode)
                {
                    var error = await response.Content.ReadAsAsync<ClientApiError>();
                    throw new ApplicationException("Api call failed: " + error.Message);
                }

                var datasets = await response.Content.ReadAsAsync<List<DatasetResponse>>();
                Console.WriteLine($"Available Datasets: {datasets.Count}");
                return datasets;
            }
        }


        public async Task<Guid> CreateJobAsync(TokenResponse token)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.SetBearerToken(token.AccessToken);

                    // Create job
                    var jobOptions = new JobCreateRequest
                    {
                        JobRef = "My Cust Ref",
                        ClientName = "My Client name",
                        JobType = "Suppress",
                        DataSets = new List<DatasetRequest>
                        {
                            new DatasetRequest {DataSetId = "REAB", MatchLevel = "IndividualLoose"},
                            new DatasetRequest {DataSetId = "REAM", MatchLevel = "IndividualLoose"},
                            new DatasetRequest {DataSetId = "REAA", MatchLevel = "IndividualLoose"},
                            new DatasetRequest {DataSetId = "REAO", MatchLevel = "IndividualLoose"},
                            new DatasetRequest {DataSetId = "REAT", MatchLevel = "IndividualLoose"}
                        }
                    };
                    var stringPayload = await Task.Run(() => JsonSerializer.Serialize(jobOptions));
                    var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                    var httpResponse = await httpClient.PostAsync($"{ApiUrl}/job", httpContent);
                    if (!httpResponse.IsSuccessStatusCode)
                    {
                        var error = await httpResponse.Content.ReadAsAsync<ClientApiError>();
                        throw new ApplicationException("Api call failed: " + error.Message);
                    }

                    var content = await httpResponse.Content.ReadAsStringAsync();
                    var jobId = content.Replace("\"", "");

                    Console.WriteLine($"Created Job;   Id: {jobId}");

                    return Guid.Parse(jobId);
                }

            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }


        public async Task<Uri> PostChunkAsync(Guid jobId, TokenResponse token)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.SetBearerToken(token.AccessToken);
                var personList = new List<ChunkRequestPerson>
                {
                    new ChunkRequestPerson
                    {
                        CustomerUrn = "URN0001", Title = "Mr", Forename1 = "Fred", Surname = "Smith",
                        Address1 = "1 The High Street", Address2 = "AnyTown", Address3 = "AB1 2CD"
                    },
                    new ChunkRequestPerson
                    {
                        CustomerUrn = "URN0002", Surname = "mrs alice smith", Address1 = "2 The High Street",
                        Address3 = "AB1 2CD"
                    },
                };

                var chunkRequest = new ChunkRequest
                {
                    JobId = jobId,
                    People = personList
                };

                var payload = await Task.Run(() => JsonSerializer.Serialize(chunkRequest));
                var httpContent = new StringContent(payload, Encoding.UTF8, "application/json");
                var httpResponse = await httpClient.PostAsync($"{ApiUrl}/chunk", httpContent);

                if (!httpResponse.IsSuccessStatusCode)
                {
                    var error = await httpResponse.Content.ReadAsAsync<ClientApiError>();
                    throw new ApplicationException(error.Message);
                }

                // could return chunk id
                var content = await httpResponse.Content.ReadAsStringAsync();
                var chunkId = content.Replace("\"", "");
                Console.WriteLine($"Submitted Chunk; Id: {chunkId}");

                var reportUri = httpResponse.Headers.Location;
                return reportUri;
            }
        }


        public async Task<Uri> PollForChunkReportAsync(Uri reportUri, TokenResponse token)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.SetBearerToken(token.AccessToken);

                var chunkPoller = new ChunkPoller(httpClient);
                var chunkPollerResults = await chunkPoller.Poll<ChunkReportResponse>(reportUri);
                var chunkReportResponse = (ChunkReportResponse) chunkPollerResults.ChunkResponse;

                Console.WriteLine(
                    $"Retrieved Chunk Report; Id: {chunkReportResponse.ChunkId} matched: {chunkReportResponse.ChunkMatchQuantity}");

                Console.WriteLine("    Chunk Results:");
                Console.WriteLine($"    ChunkId:        {chunkReportResponse.ChunkId}");
                Console.WriteLine($"    JobId:          {chunkReportResponse.JobId}");
                Console.WriteLine($"    Job Ref:        {chunkReportResponse.JobRef}");
                Console.WriteLine($"    Job Type:       {chunkReportResponse.JobType}");
                Console.WriteLine($"    Chunk Input:    {chunkReportResponse.ChunkInputQuantity}");
                Console.WriteLine($"    Chunk Matches:  {chunkReportResponse.ChunkMatchQuantity}");
                Console.WriteLine($"    Iteration:      {chunkReportResponse.Iteration}");
                Console.WriteLine();
                if (chunkReportResponse.DatasetMatches.Count > 0)
                {
                    Console.WriteLine("    Matches:");
                    foreach (var datasetMatch in chunkReportResponse.DatasetMatches)
                    {
                        Console.WriteLine($"        {datasetMatch.DatasetId}  {datasetMatch.Quantity}");
                    }
                }

                var downloadUri = chunkPollerResults.Location;
                return downloadUri;
            }
        }


        public async Task<ChunkDownloadResponse> PollForChunkResultsAsync(Uri downloadUri, TokenResponse token)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.SetBearerToken(token.AccessToken);

                var chunkPoller = new ChunkPoller(httpClient);
                var chunkPollerResults = await chunkPoller.Poll<ChunkDownloadResponse>(downloadUri);
                var chunkResponse = (ChunkDownloadResponse) chunkPollerResults.ChunkResponse;

                Console.WriteLine(
                    $"Retrieved Chunk Download; Id: {chunkResponse.ChunkId} matched: {chunkResponse.PersonMatches.Count}");

                Console.WriteLine(
                    "--------------------------------------------------------------------------------");
                Console.WriteLine("Results:");
                foreach (var chunkResponseResult in chunkResponse.PersonMatches)
                {
                    Console.WriteLine("    ----------------------------------------");
                    Console.WriteLine($"    CustomerUrn:   {chunkResponseResult.CustomerUrn}");

                    // matches
                    if (chunkResponseResult.Datasets?.Count > 0)
                    {
                        Console.WriteLine("    Datasets:");
                        foreach (var datasetMatch in chunkResponseResult.Datasets)
                        {
                            var matchList = new StringBuilder();
                            Console.WriteLine($"        DatasetId: {datasetMatch.DatasetId},");
                            Console.WriteLine($"        MatchLevel: {datasetMatch.MatchLevel},");
                            Console.WriteLine("        Attributes:");
                            foreach (var attribute in datasetMatch.Attributes)
                            {
                                var attributeValues = new StringBuilder();
                                foreach (var value in attribute.Value)
                                {
                                    attributeValues.Append($"{value},");
                                }

                                Console.WriteLine($"            {attribute.Key}:{attributeValues}");
                            }
                        }
                    }

                    Console.WriteLine("    ----------------------------------------");
                }

                Console.WriteLine(
                    "--------------------------------------------------------------------------------");

                return chunkResponse;
            }
        }
    }
}