﻿using System;
using System.Collections.Generic;

namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    /// <summary>
    /// Job settings returned.
    /// </summary>
    public class JobResponse
    {
        // Must have parameter-less constructor
        public JobResponse() { }
        public Guid JobId { get; set; }
        public string JobType { get; set; }
        public string JobRef { get; set; }
        public string ClientName { get; set; }
        public List<DatasetRequest> DataSets { get; set; }
    }
}
