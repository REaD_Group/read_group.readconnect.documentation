﻿namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    /// <inheritdoc />
    /// <summary>
    /// Create job settings.
    /// </summary>
    public class JobCreateRequest : JobRequest
    {
        // Must have parameter-less constructor
        public JobCreateRequest() { }
        
    }
}
