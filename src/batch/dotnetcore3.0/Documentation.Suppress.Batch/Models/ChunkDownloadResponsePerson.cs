﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    /// <summary>
    /// Job chunk person response.
    /// </summary>
    public class ChunkDownloadResponsePerson
    {
        // Must have parameter-less constructor
        public ChunkDownloadResponsePerson() { }

        /// <summary>
        /// Unique identifier for this person.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string CustomerUrn { get; set; }

        public List<ChunkDownloadResponseDataset> Datasets { get; set; }
    }
}
