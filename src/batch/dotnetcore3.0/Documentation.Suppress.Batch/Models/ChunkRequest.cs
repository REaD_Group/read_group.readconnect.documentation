﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    /// <summary>
    /// Job chunk sent by the customer.
    /// </summary>
    public class ChunkRequest
    {
        // Must have parameter-less constructor
        public ChunkRequest() { }

        /// <summary>
        /// The job id.
        /// </summary>
        [Required]
        public Guid JobId { get; set; }

        /// <summary>
        /// List of names and addresses to run against suppression pool.
        /// </summary>
        [Required]
        public List<ChunkRequestPerson> People { get; set; }
    }

}
