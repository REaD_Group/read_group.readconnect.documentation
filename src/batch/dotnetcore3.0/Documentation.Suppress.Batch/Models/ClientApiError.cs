﻿namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    public class ClientApiError
    {
        public string Message { get; set; }
    }
}
