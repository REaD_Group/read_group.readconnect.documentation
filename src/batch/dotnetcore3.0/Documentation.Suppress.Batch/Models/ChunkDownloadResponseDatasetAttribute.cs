﻿using System.Collections.Generic;

namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    public class ChunkDownloadResponseDatasetAttribute
    {
        public string Key { get; set; }
        public List<string> Value { get; set; }
    }
}
