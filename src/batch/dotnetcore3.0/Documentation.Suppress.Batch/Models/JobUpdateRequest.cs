﻿namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    /// <inheritdoc />
    /// <summary>
    /// Update job settings.
    /// </summary>
    public class JobUpdateRequest : JobRequest
    {
        // Must have parameter-less constructor
        public JobUpdateRequest() { }

    }
}
