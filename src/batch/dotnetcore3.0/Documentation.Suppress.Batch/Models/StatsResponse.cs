﻿namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    /// <summary>
    /// System statistics returned by the api.
    /// </summary>
    public class StatsResponse
    {
        // Must have parameter-less constructor
        public StatsResponse() { }

        /// <summary>
        /// The time in seconds that the last chunk spent queuing
        /// </summary>
        public double QueueWaitDuration { get; set; }

        /// <summary>
        /// The time in seconds that the last chunk took to process.
        /// </summary>
        public double ProcessingWaitDuration { get; set; }
    }
}
