﻿using System.Collections.Generic;

namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    /// <summary>
    /// Job chunk report.
    /// </summary>
    public class ChunkReportResponse : ChunkResponse
    {
        // Must have parameter-less constructor
        public ChunkReportResponse() {}

        public List<ChunkReportDatasetResponse> DatasetMatches { get; set; }
    }
}
