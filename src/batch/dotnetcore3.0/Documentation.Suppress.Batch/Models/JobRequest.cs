﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    /// <summary>
    /// Job settings.
    /// </summary>
    public abstract class JobRequest
    {
        /// <summary>
        /// Either Flag or Suppress
        /// </summary>
        public string JobType { get; set; }

        /// <summary>
        /// Your job reference
        /// </summary>
        [StringLength(50)]
        public string JobRef { get; set; }

        /// <summary>
        /// Your clients name
        /// </summary>
        [StringLength(50)]
        public string ClientName { get; set; }

        /// <summary>
        /// Unique ordered list of suppression datasets to use.
        /// </summary>
        [Required]
        public List<DatasetRequest> DataSets { get; set; }
    }
}
