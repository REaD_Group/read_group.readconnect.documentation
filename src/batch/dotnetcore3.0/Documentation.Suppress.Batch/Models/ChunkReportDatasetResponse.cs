﻿namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    public class ChunkReportDatasetResponse
    {
        // Must have parameter-less constructor
        public ChunkReportDatasetResponse() {}

        public string DatasetId { get; set; }
        public int Quantity { get; set; }
    }
}
