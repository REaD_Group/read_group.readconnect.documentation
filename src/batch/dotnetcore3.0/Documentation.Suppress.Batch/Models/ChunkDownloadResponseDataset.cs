﻿using System.Collections.Generic;

namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    public class ChunkDownloadResponseDataset
    {
        public ChunkDownloadResponseDataset()
        {
            Attributes = new List<ChunkDownloadResponseDatasetAttribute>();
        }

        public string DatasetId { get; set; }
        public string MatchLevel { get; set; }
        public List<ChunkDownloadResponseDatasetAttribute> Attributes { get; set; }
    }
}
