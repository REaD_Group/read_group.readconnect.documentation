﻿using System.Collections.Generic;

namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    /// <summary>
    /// Job chunk response.
    /// </summary>
    public class ChunkDownloadResponse : ChunkResponse
    {
        public ChunkDownloadResponse() {}

        public List<ChunkDownloadResponsePerson> PersonMatches { get; set; }
    }
}
