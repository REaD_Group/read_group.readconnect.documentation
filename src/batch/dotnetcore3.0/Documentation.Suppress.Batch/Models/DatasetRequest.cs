﻿using System.ComponentModel.DataAnnotations;

namespace SwiftCore.Documentation.Suppress.Batch.Models
{
    public class DatasetRequest
    {
        /// <summary>
        /// The DatasetId 
        /// </summary>
        [Required]
        public string DataSetId { get; set; }

        /// <summary>
        /// The match level
        /// </summary>
        [Required]
        public string MatchLevel { get; set; }
    }
}
