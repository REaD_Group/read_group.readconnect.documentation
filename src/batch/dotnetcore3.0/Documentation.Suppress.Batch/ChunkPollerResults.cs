﻿ using System;
using SwiftCore.Documentation.Suppress.Batch.Models;

namespace SwiftCore.Documentation.Suppress.Batch
{
    class ChunkPollerResults
    {
        public IChunkResponse ChunkResponse { get; set; }
        public Uri Location { get; set; }
    }
}
